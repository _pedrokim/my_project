#include <stdio.h>
#include "dice.h"

/* Função principal de rolamento de dado */
int main() {
	int n;
	initializeSeed();
	printf("How many faces does your dice have? ");
	scanf("%d", &n);
	printf("Let's roll the dice: %d\n", rollDice(n));
	return 0;
}